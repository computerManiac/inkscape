/*
 * File operations (independent of GUI)
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

SPDocument* open_file(const Glib::RefPtr<Gio::File>& file = Glib::RefPtr<Gio::File>());


